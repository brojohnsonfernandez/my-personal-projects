const apiKey = "0TXhYJ9BeOA7aT6CpVBmpR5fy8-vwKzTIEZPp1eESbQ";

const formEl = document.querySelector('form');
const inputEl = document.getElementById("search-input");
const searchImage = document.querySelector(".search-images");
const showMore = document.getElementById("show-btn");

let page = 1;
let inputData = "";

async function searchValue() {
    inputData = inputEl.value;

    if(inputEl.value === ""){
        error_message.textContent= "Please Enter Some Search Value for Output";
        return;
    }else{
        error_message.textContent = "";

    }
    const url = `https://api.unsplash.com/search/photos?page=${page}&query=${inputData}&client_id=${apiKey}`;

    const response = await fetch(url)
    const data = await response.json()

    const results = data.results;

    if (page === 1) {
        searchImage.innerHTML = ""
    }

    if (results && Array.isArray(results)) {
        results.map((result) => {
            const imageWrapper = document.createElement('div');
            imageWrapper.classList.add('image-result');
            const image = document.createElement('img');
            image.src = result.urls.small;
            image.alt = result.alt_description;
            const imageLink = document.createElement('a');
            imageLink.href = result.links.html;
            imageLink.target = "_blank";
            imageLink.textContent = result.alt_description;

            imageWrapper.appendChild(image);
            imageWrapper.appendChild(imageLink);
            searchImage.appendChild(imageWrapper);
        });
        page++;
        if (page > 1) {
            showMore.style.display = "block"
        }
    }
    else {
        console.error('Invalid data structure received from the API');
    }
   
};


formEl.addEventListener('submit', (e) => {
    e.preventDefault();
    page = 1;
    searchValue();
});

showMore.addEventListener('click', () => {
    searchValue();
});